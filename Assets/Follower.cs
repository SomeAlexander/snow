﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class Follower : MonoBehaviour
{
    public Transform targert;
    public Vector3 offset;

    private void LateUpdate() {
        if(targert != null) {
            this.transform.position = targert.position + offset;
        }
    }
}
