﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Camera))]
public class DepthCamera : MonoBehaviour
{
    public Vector3 scale;
    private new Camera camera;

    [ContextMenu("Update Matrix")]
    public void Awake() {
        camera = GetComponent<Camera>();
        camera.ResetWorldToCameraMatrix();
        camera.ResetProjectionMatrix();
        camera.projectionMatrix = camera.projectionMatrix * Matrix4x4.Scale(scale);
        Debug.Log(camera.projectionMatrix);
    }

    private void OnPreRender() {
        GL.invertCulling = true;
    }

    private void OnPostRender() {
        GL.invertCulling = false;
    }

}
