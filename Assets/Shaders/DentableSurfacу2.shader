﻿Shader "IndentSurface/DentableSurfac2" {
 Properties {
     _Color ("Color", Color) = (1,1,1,1)
     _MainTex ("Albedo (RGB)", 2D) = "white" {}
     _SpecularMap("Specular", 2D) = "black" {}
	 _Smoothness("Smoothness", Range(0,1)) = 0

     _NormalMap ("Normal Map", 2D) = "normal" {}
	 _NormalPower(" ",float) = 1

     _Indentmap ("Indentation", 2d) = "white" {}
	 _MaxDepth("Max Depth", 2d) = "black" {}
	 _Tess("Tessellation", Range(1,96)) = 4
     _IndentDepth ("Indentation Depth", Range(0,10)) = 1.0
 }
 SubShader {
     Tags { "RenderType"="Opaque" }
     LOD 200
     
     CGPROGRAM


     // Physically based Standard lighting model, and enable shadows on all light types
        #pragma surface surf StandardSpecular fullforwardshadows vertex:vert addshadow tessellate:tessDistance
        #include "Tessellation.cginc"

     // Use shader model 3.0 target, to get nicer looking lighting
     #pragma target 3.0

        sampler2D _MainTex;
        sampler2D _Indentmap;
        sampler2D _SpecularMap;
		sampler2D _NormalMap;
		sampler2D _MaxDepth;
		half _Glossiness;
		half _Metallic;
		fixed4 _Color;
		float _Tess;
		float _IndentDepth;
		float _NormalPower;
		fixed _Smoothness;

     struct Input {
            float2 uv_MainTex;
            float3 normal;
     };

        struct appdata {
            float4 vertex : POSITION;
            float4 tangent : TANGENT;
            float3 normal : NORMAL;
            float2 texcoord : TEXCOORD0;
        };

        float4 tessFixed()
        {
            return _Tess;
        }

        float4 tessDistance(appdata v0, appdata v1, appdata v2) {
            float minDist = 0.0;
            float maxDist = 100.0;
            return UnityDistanceBasedTess(v0.vertex, v1.vertex, v2.vertex, minDist, maxDist, _Tess);
        }
	
		float3 calcNormal(sampler2D tex, float2 uv)
		{
			float u = 1.0/2048.0;
			fixed hts_0 = tex2Dlod(tex, float4(uv + float2(-u, 0), 0, 0)).x;
			fixed hts_1 = tex2Dlod(tex, float4(uv + float2(u, 0), 0, 0)).x;
			fixed hts_2 = tex2Dlod(tex, float4(uv + float2(0, -u), 0, 0)).x;
			fixed hts_3 = tex2Dlod(tex, float4(uv + float2(0, u), 0, 0)).x;
			float2 _step = float2(1.0, 0.0);
			float3 va = normalize(float3(_step.xy, hts_1 - hts_0));
			float3 vb = normalize(float3(_step.yx, hts_3 - hts_2));
			return cross(va, vb); 
		}


        void vert(inout appdata v) {
			fixed4 uv = fixed4(v.texcoord.xy, 0, 0);
            fixed height = tex2Dlod(_Indentmap, uv).r;
			fixed maxDepth = tex2Dlod(_MaxDepth, uv).r;
            v.vertex.xyz = v.vertex.xyz  + v.normal*height*maxDepth * _IndentDepth;
			v.normal = normalize(v.normal+ calcNormal(_Indentmap, v.texcoord.xy));
        }

		

     void surf (Input IN, inout SurfaceOutputStandardSpecular o) {
         fixed4 c = tex2D (_MainTex, IN.uv_MainTex) * _Color;
         o.Albedo = c.rgb;
			o.Alpha = c.a;
			o.Specular = tex2D(_SpecularMap, IN.uv_MainTex);
            o.Smoothness = _Smoothness;
			o.Occlusion = 1;
            

			fixed3 normal = UnpackScaleNormal(tex2D(_NormalMap, IN.uv_MainTex), _NormalPower);
			o.Normal = normalize(normal);
			//o.Emission = (o.Normal.r)*0.3;
     }
     ENDCG
 }
 FallBack "Diffuse"
}
