﻿Shader "IndentSurface/IndentStamp_Regeneration" {

	Properties{
		
	}
		
	SubShader {

		Tags {
			"Queue"="Transparent"
			"IgnoreProjector"="True"
			"RenderType"="Transparent"
			"PreviewType"="Plane"
		}
		Lighting Off Cull Off ZTest Always ZWrite Off
		Blend SrcAlpha OneMinusSrcAlpha

		Pass {
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma multi_compile _ STEREO_INSTANCING_ON 
			#pragma multi_compile _ UNITY_SINGLE_PASS_STEREO
			
			#include "UnityCG.cginc"
			

			uniform sampler2D _MainTex;

			struct appdata_t {
				float4 vertex : POSITION;
				float2 texcoord : TEXCOORD0;
			};

			struct v2f {
				float4 vertex : SV_POSITION;
				float2 texcoord : TEXCOORD0;
			};

			v2f vert(appdata_t v)
			{
				v2f o;
				UNITY_SETUP_INSTANCE_ID(v);
				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(o);
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.texcoord = v.texcoord, _MainTex;
				return o;
			}

			fixed4 frag (v2f i) : SV_Target
			{
				fixed d = 0.01;
				fixed4 surface = tex2D(_MainTex, i.texcoord);
				return surface+d;
			}
			ENDCG
		}
	}
}
